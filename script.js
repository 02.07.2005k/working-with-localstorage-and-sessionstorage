// Теоретичні питання

// 1) localStorage: Зберігає дані постійно, доступні для всіх вкладок/вікон одного домену.
// sessionStorage: Зберігає дані лише на час сеансу, дані видаляються при закритті вкладки/вікна.

// 2) Ризик XSS-атак: Зловмисники можуть отримати доступ до даних через уразливості.
// Відсутність шифрування: Дані зберігаються у відкритому вигляді.
// Сторонні скрипти та розширення: Можуть мати доступ до даних.

// 3) Дані видаляються при закритті вкладки або вікна браузера.

// Практичне завдання 

document.addEventListener('DOMContentLoaded', () => {
    const themeToggleBtn = document.getElementById('theme-toggle');
    
    if (localStorage.getItem('theme') === 'dark') {
        document.body.classList.add('dark-mode');
    }

    themeToggleBtn.addEventListener('click', () => {
        document.body.classList.toggle('dark-mode');

        if (document.body.classList.contains('dark-mode')) {
            localStorage.setItem('theme', 'dark');
        } else {
            localStorage.removeItem('theme');
        }
    });
});